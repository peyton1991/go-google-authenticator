package gogoogleauthenticator

import (
	"fmt"
	"testing"
)

func TestGoogleAuth_GetSecret(t *testing.T) {
	secret := NewGoogleAuth().GetSecret()
	fmt.Println(secret)
	t.Log("secret:",secret)
}

func TestGoogleAuth_GetCode(t *testing.T) {
	code,err := NewGoogleAuth().GetCode("JTHAZBXA4GL2KOWCN2ZBXRO4PXMFXUUC")
	if err !=nil{
		t.Error(err)
		return
	}
	t.Log("cod:",code)
}

func TestGoogleAuth_VerifyCode(t *testing.T) {
	ok,err:=NewGoogleAuth().VerifyCode("JTHAZBXA4GL2KOWCN2ZBXRO4PXMFXUUC","274480")
	if err!=nil{
		t.Error(err)
		return
	}
	if !ok{
		t.Error("faild")
		return
	}else{
		t.Log("seccess!")
		return
	}
}